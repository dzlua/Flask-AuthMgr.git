4: v2.0.0
    python 3.7

3: v1.1.1

Exposed interface:
    secret_key
    aexpires_in
    rfexpires_in
    uexpires_in

2: v1.1.0

add user_token
API:
    generate_user_token
    user_token_data
    verify_user_token
    required_user_token
    error_handler_user_token

1: v1.0.0
2017-11-05 16:42
