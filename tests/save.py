#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : tests
* file   : save.py
* time   : 2017-11-04 14:45:40
'''
#--------------------#
from nosave import TestNoSave
#--------------------#

#--------------------#
class TestSave(TestNoSave):
    def initAuthSet(self, app, auth):
        TestNoSave.initAuthSet(self, app, auth)
        @auth.set_save_token
        def save_token(tdata):
            if tdata['type'] == 'user_token':
                email = tdata['data']['email']
                self.tokens['user_token'][email] = tdata['token']
                return True
            user = tdata['data']['user']
            tp = tdata['type']
            self.tokens[tp][user] = tdata['token']
            return True
        @auth.set_get_token
        def get_token(tdata):
            if tdata['type'] == 'user_token':
                email = tdata['data']['email']
                return self.tokens['user_token'][email]
            user = tdata['data']['user']
            tp = tdata['type']
            return self.tokens[tp][user]
    def verifyRfToken(self, token, user):
        if self.tokens['refresh_token'][user] != token:
            return False
        return self.CheckNeedRfToken(token)
    def verifyAToken(self, token, user):
        if self.tokens['access_token'][user] != token:
            return False
        return self.CheckNeedAToken(token)
    def test_save_old_rftoken(self):
        token_old = self.gettoken()
        self.assertTrue(self.CheckNeedRfToken(token_old['refresh_token']))
        token_new = self.gettoken()
        self.assertTrue(self.CheckNeedRfToken(token_new['refresh_token']))
        self.assertFalse(self.CheckNeedRfToken(token_old['refresh_token']))
    def test_save_old_atoken(self):
        token_old = self.gettoken()
        self.assertTrue(self.CheckNeedAToken(token_old['access_token']))
        token_new = self.gettoken()
        self.assertTrue(self.CheckNeedAToken(token_new['access_token']))
        self.assertFalse(self.CheckNeedAToken(token_old['access_token']))
#--------------------#
