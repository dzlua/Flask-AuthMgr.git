#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : tests
* file   : nosave.py
* time   : 2017-11-03 17:57:44
'''
#--------------------#
from base import TestBase
from flask import g
import time
#--------------------#

#--------------------#
class TestNoSave(TestBase):
    def initAuth(self, app, auth):
        TestBase.initAuth(self, app, auth)
        self.expires_in = 2
    def verifyRfToken(self, token, user):
        return self.CheckNeedRfToken(token)
    def verifyAToken(self, token, user):
        return self.CheckNeedAToken(token)
    def verifyUToken(self, token, user):
        return self.CheckNeedUToken(token)
    def test_rftoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertTrue(self.CheckRfToken())
        self.assertFalse(self.CheckRfToken(token['refresh_token']))
        self.assertFalse(self.CheckRfToken(token['access_token']))
        self.assertFalse(self.CheckRfToken(utoken['user_token']))
    def test_atoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertFalse(self.CheckAToken())
        self.assertTrue(self.CheckAToken(token['refresh_token']))
        self.assertFalse(self.CheckAToken(token['access_token']))
        self.assertFalse(self.CheckAToken(utoken['user_token']))
    def test_need_pwd(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertTrue(self.CheckNeedPwd())
        self.assertFalse(self.CheckNeedPwd(token['refresh_token']))
        self.assertFalse(self.CheckNeedPwd(token['access_token']))
        self.assertFalse(self.CheckNeedPwd(utoken['user_token']))
    def test_need_rftoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertFalse(self.CheckNeedRfToken())
        self.assertTrue(self.CheckNeedRfToken(token['refresh_token']))
        self.assertFalse(self.CheckNeedRfToken(token['access_token']))
        self.assertFalse(self.CheckNeedRfToken(utoken['user_token']))
    def test_need_pwd_rftoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertTrue(self.CheckNeedPwdOrRfToken())
        self.assertTrue(self.CheckNeedPwdOrRfToken(token['refresh_token']))
        self.assertFalse(self.CheckNeedPwdOrRfToken(token['access_token']))
        self.assertFalse(self.CheckNeedPwdOrRfToken(utoken['user_token']))
    def test_need_atoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertFalse(self.CheckNeedAToken())
        self.assertFalse(self.CheckNeedAToken(token['refresh_token']))
        self.assertTrue(self.CheckNeedAToken(token['access_token']))
        self.assertFalse(self.CheckNeedAToken(utoken['user_token']))
    def test_need_pwd_atoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertTrue(self.CheckNeedPwdOrAToken())
        self.assertFalse(self.CheckNeedPwdOrAToken(token['refresh_token']))
        self.assertTrue(self.CheckNeedPwdOrAToken(token['access_token']))
        self.assertFalse(self.CheckNeedPwdOrAToken(utoken['user_token']))
    def test_need_utoken(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertFalse(self.CheckNeedUToken())
        self.assertFalse(self.CheckNeedUToken(token['refresh_token']))
        self.assertFalse(self.CheckNeedUToken(token['access_token']))
        self.assertTrue(self.CheckNeedUToken(utoken['user_token']))
    #----------#
    def test_token_equal(self):
        token1 = self.gettoken()
        token2 = self.gettoken()
        token3 = self.gettoken()
        self.assertNotEqual(token1['refresh_token'], token2['refresh_token'])
        self.assertNotEqual(token1['refresh_token'], token3['refresh_token'])
        self.assertNotEqual(token2['refresh_token'], token3['refresh_token'])
        self.assertNotEqual(token1['access_token'], token2['access_token'])
        self.assertNotEqual(token1['access_token'], token3['access_token'])
        self.assertNotEqual(token2['access_token'], token3['access_token'])
        utoken1 = self.getutoken()
        utoken2 = self.getutoken()
        utoken3 = self.getutoken()
        self.assertNotEqual(utoken1['user_token'], utoken2['user_token'])
        self.assertNotEqual(utoken1['user_token'], utoken3['user_token'])
        self.assertNotEqual(utoken2['user_token'], utoken3['user_token'])
    def test_expire_in(self):
        token = self.gettoken()
        utoken = self.getutoken()
        self.assertTrue(self.CheckNeedRfToken(token['refresh_token']))
        self.assertTrue(self.CheckNeedAToken(token['access_token']))
        self.assertTrue(self.CheckNeedUToken(utoken['user_token']))
        #
        # waite self.expires_in time
        time.sleep(self.expires_in+1)
        #
        self.assertFalse(self.CheckNeedRfToken(token['refresh_token']))
        self.assertFalse(self.CheckNeedAToken(token['access_token']))
        self.assertFalse(self.CheckNeedUToken(utoken['user_token']))
#--------------------#
