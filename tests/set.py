#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : tests
* file   : set.py
* time   : 2017-11-04 16:07:20
'''
#--------------------#
from save import TestSave
import base64, time
#--------------------#

#--------------------#
class TestSet(TestSave):
    def initAuthSet(self, app, auth):
        TestSave.initAuthSet(self, app, auth)
        #
        @auth.set_decode_password
        def decode_password(pwd):
            return base64.b64encode(pwd)
        #
        @auth.set_check_password
        def check_password(dpwd, pwd):
            return decode_password(pwd) == dpwd
        #
        @auth.set_generate_token
        def generate_token(secret_key, expires_in, data):
            s = secret_key + str(int(expires_in)) + \
                '=' + str(int(time.time() + expires_in)) + \
                '=' + str(data)
            return base64.b64encode(s)
        #
        @auth.set_parser_token
        def parser_token(secret_key, token):
            if not token or token.strip() == '':
                return None
            #
            try:
                s = base64.b64decode(token)
                ss = s.split('=')
                exp = int(ss[1])
                if exp < int(time.time()):
                    return None
                return eval(ss[2])
            except:
                return None
    def initAuthAddRoutes(self, app, auth):
        TestSave.initAuthAddRoutes(self, app, auth)

        @app.route('/getpwd')
        @auth.required_login
        def route_getpwd():
            return self.users['susan']
    def test_set_pwd(self):
        pwd = self.auth.decode_password('susanpassword')
        pwd_get = self.get('getpwd')
        self.assertIsNotNone(pwd_get)
        self.assertEqual(pwd, pwd_get)
    def test_set_token(self):
        token_get = self.gettoken()
        rftoken = self.tokens['refresh_token']['susan']
        atoken = self.tokens['access_token']['susan']
        self.assertEqual(token_get['refresh_token'], rftoken)
        self.assertEqual(token_get['access_token'], atoken)
#--------------------#
