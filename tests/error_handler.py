#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : tests
* file   : error_handler.py
* time   : 2017-11-05 16:00:27
'''
#--------------------#
from nosave import TestNoSave
#--------------------#

#--------------------#
class TestErrorHandler(TestNoSave):
    def initAuthSet(self, app, auth):
        TestNoSave.initAuthSet(self, app, auth)
        self.error_responds = 'fuck off'
    def initAuthErrorHandler(self, app, auth):
        @auth.error_handler_login
        @auth.error_handler_refresh_token
        @auth.error_handler_access_token
        def handle_error():
            return self.error_responds
#--------------------#
