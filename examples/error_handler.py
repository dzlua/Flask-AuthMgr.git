#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : examples
* file   : error_handler.py
* time   : 2017-11-05 16:16:10
'''
#--------------------#
import base
from flask import Flask, g
from flask_auth_mgr import AuthMgr
#--------------------#

#--------------------#
app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
auth = AuthMgr(app.config['SECRET_KEY'])
#--------------------#

#--------------------#
users = {
    "john": auth.decode_password("johnpassword"),
    "susan": auth.decode_password("susanpassword")
}
#--------------------#

#--------------------#
@auth.verify_password
def verify_password(user, pwd):
    g.user = None
    if user not in users:
        return False
    if auth.check_password(users.get(user), pwd):
        g.user = user
        return True
    return False

@auth.verify_refresh_token
def verify_rftoken(token):
    data = auth.refresh_token_data(token)
    if data is None:
        return False
    g.user = data['user']
    return True

@auth.verify_access_token
def verify_atoken(token):
    data = auth.access_token_data(token)
    if data is None:
        return False
    g.user = data['user']
    return True
#--------------------#

#--------------------#
@auth.error_handler_login
def handle_error_login():
    return 'login error'

@auth.error_handler_access_token
def handle_error_access_token():
    return 'access token error'

@auth.error_handler_refresh_token
def handle_error_refresh_token():
    return 'refresh token error'
#--------------------#

#--------------------#
base.addroutes(app, auth)
#--------------------#

#--------------------#
if __name__ == '__main__':
    app.run(debug=True)
#--------------------#
