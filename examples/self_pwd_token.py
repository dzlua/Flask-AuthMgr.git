#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : examples
* file   : self_pwd_token.py
* time   : 2017-11-03 14:15:40
'''
#--------------------#
import base
from flask import Flask, g
from flask_auth_mgr import AuthMgr

import base64, time
#--------------------#

#--------------------#
app = Flask(__name__)
app.config['SECRET_KEY'] = 'the quick brown fox jumps over the lazy dog'
auth = AuthMgr(app.config['SECRET_KEY'])
#--------------------#

#--------------------#
base.addroutes(app, auth)
#--------------------#

#--------------------#
@auth.set_decode_password
def decode_password(pwd):
    return base64.b64encode(pwd)
@auth.set_check_password
def check_password(dpwd, pwd):
    return decode_password(pwd) == dpwd
@auth.set_generate_token
def generate_token(secret_key, expires_in, data):
    s = secret_key + str(int(expires_in)) + \
        '=' + str(int(time.time() + expires_in)) + \
        '=' + str(data)
    return base64.b64encode(s)
#
@auth.set_parser_token
def parser_token(secret_key, token):
    if not token or token.strip() == '':
        return None
    #
    try:
        s = base64.b64decode(token)
        ss = s.split('=')
        exp = int(ss[1])
        if exp < int(time.time()):
            return None
        return eval(ss[2])
    except:
        return None
#--------------------#

#--------------------#
users = {
    "john": auth.decode_password("johnpassword"),
    "susan": auth.decode_password("susanpassword")
}

tokens = {
    'refresh_token': {},
    'access_token': {},
    'user_token': {}
}
#--------------------#

#--------------------#
@auth.set_save_token
def save_token(tdata):
    if tdata['type'] == 'user_token':
        email = tdata['data']['email']
        tokens['user_token'][email] = tdata['token']
        return True
    user = tdata['data']['user']
    tp = tdata['type']
    tokens[tp][user] = tdata['token']
    return True
@auth.set_get_token
def get_token(tdata):
    if tdata['type'] == 'user_token':
        email = tdata['data']['email']
        return tokens['user_token'][email]
    user = tdata['data']['user']
    tp = tdata['type']
    return tokens[tp][user]
#--------------------#

#--------------------#
@auth.verify_password
def verify_password(user, pwd):
    g.user = None
    if user not in users:
        return False
    if auth.check_password(users.get(user), pwd):
        g.user = user
        return True
    return False

@auth.verify_refresh_token
def verify_rftoken(token):
    data = auth.refresh_token_data(token)
    if data is None:
        return False
    g.user = data['user']
    return True

@auth.verify_access_token
def verify_atoken(token):
    data = auth.access_token_data(token)
    if data is None:
        return False
    g.user = data['user']
    return True

@auth.verify_user_token
def verify_utoken(token):
    data = auth.user_token_data(token)
    if data is None:
        return False
    g.email = data['email']
    return True
#--------------------#

#--------------------#
if __name__ == '__main__':
    app.run(debug=True)
#--------------------#
