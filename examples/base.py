#coding:utf-8
'''
* coder  : dzlua
* email  : 505544956@qq.com
* module : Flask-AuthMgr
* path   : examples
* file   : base.py
* time   : 2017-11-03 14:00:26
'''
#--------------------#
from flask import g, jsonify
#--------------------#

#--------------------#
def addroutes(app, auth):
    @app.route('/refresh_token')
    @auth.required_login
    def route_refresh_token():
        token_data = {
            'user': g.user,
        }

        data = {
            'refresh_token': auth.generate_refresh_token(token_data),
        }
        return jsonify(data)

    @app.route('/access_token')
    @auth.required_refresh_token
    def route_access_token():
        token_data = {
            'user': g.user,
        }

        data = {
            'access_token': auth.generate_access_token(token_data),
        }
        return jsonify(data)

    # rftoken -> rftoken
    @app.route('/token')
    @auth.required_multi_refresh
    def route_token():
        token_data = {
            'user': g.user,
        }

        data = {
            'refresh_token': auth.generate_refresh_token(token_data),
            'access_token': auth.generate_access_token(token_data),
        }
        return jsonify(data)
    @app.route('/user_token')
    def route_user_token():
        token_data = {
            'email': '505544956@qq.com'
        }

        data = {
            'user_token': auth.generate_user_token(token_data)
        }
        return jsonify(data)
    #--------------------#
    @app.route('/need_pwd')
    @auth.required_login
    def route_need_pwd():
        return 'this page need password: %s\n' % g.user

    @app.route('/need_rftoken')
    @auth.required_refresh_token
    def route_need_rftoken():
        return 'this page need refresh token: %s\n' % g.user

    @app.route('/need_pwd_rftoken')
    @auth.required_multi_refresh
    def route_need_pwd_rftoken():
        return 'this page need password or refresh token: %s\n' % g.user

    @app.route('/need_atoken')
    @auth.required_access_token
    def route_need_atoken():
        return 'this page need access token: %s\n' % g.user

    @app.route('/need_pwd_atoken')
    @auth.required_multi_access
    def route_need_pwd_atoken():
        return 'this page need password or access token: %s\n' % g.user
    
    @app.route('/need_utoken')
    @auth.required_user_token
    def route_need_utoken():
        return g.email
#--------------------#
    